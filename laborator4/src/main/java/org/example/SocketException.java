package org.example;

public class SocketException extends Exception {
    public SocketException(String message) {
        super(message);
    }
}
