package org.example;

import javax.json.Json;
import java.io.*;
import java.net.Socket;

public class MessageSender extends Thread {
    private volatile boolean running = true;
    private ConnectionManager connectionManager;
    private Socket socket;
    private PrintWriter printWriter;

    public MessageSender(Socket socket, ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
        this.socket = socket;
        try {
            this.printWriter = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            System.out.println("Error: Initializing  Connection");
        }
    }


//Closes the socket and the associated PrintWriter.
//Sets the running flag to false.
//Throws a SocketException to indicate that the peer disconnected.
    public void closeSocket() throws SocketException {
        try {
            running = false;
            if (socket != null && !socket.isClosed()) {
                socket.close();
                printWriter.close();
                throw new SocketException("Socket Exception: Peer disconnected.");
            }
        } catch (SocketException e) {
            System.out.println("Error while closing socket: " + e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void run() {
        try {
            // Create a BufferedReader to read from the socket's input stream
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));

            // Create a PrintWriter to write to the socket's output stream
            this.printWriter = new PrintWriter(socket.getOutputStream(), true);

            // Create a StringWriter to construct a JSON object for an initial acknowledgment message
            StringWriter stringWriter = new StringWriter();
            Json.createWriter(stringWriter).writeObject(Json.createObjectBuilder()
                    .add("username", "System")
                    .add("message", "!ack")
                    .build());

            // Send the acknowledgment message to the connected peer
            printWriter.println(stringWriter.toString());
            System.out.println("Sent !ack to a connected peer");

            // Continue reading messages from the server while the 'running' flag is true
            while (running) {
                // Read a line from the BufferedReader, representing a message from the server
                connectionManager.sendMessage(bufferedReader.readLine());
            }
        } catch (IOException e) {
            // Handle IOException that might occur during socket communication
            e.printStackTrace();

            // Remove the current MessageSender from the ConnectionManager's list if an exception occurs
            connectionManager.getMessageSenders().remove(this);
        }
    }


    public Socket getSocket() {
        return socket;
    }

    public PrintWriter getPrintWriter() {
        return printWriter;
    }
}
