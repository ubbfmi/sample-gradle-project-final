package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import javax.json.Json;
import javax.json.JsonObject;

public class MessageReader extends Thread{
    private BufferedReader bufferedReader;

    public MessageReader(Socket socket) throws IOException {
        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }
//It runs an infinite loop to continuously read JSON objects
// from the server using a JsonReader created from the bufferedReader
    public void run() {
        boolean flag = true;
        while (flag) {
            try {
                JsonObject jsonObject = Json.createReader(bufferedReader).readObject();
                if (jsonObject.containsKey("username")){
                    System.out.println("["+jsonObject.getString("username")+"]: "+jsonObject.getString("message"));
                }
            } catch (Exception e) {
                flag = false;
                interrupt();
                e.printStackTrace();
            }
        }
    }
}
